module Main where

import           Control.Monad         (forever)
import           Data.ByteString.Char8 (pack)
import           Livecoin              (ResponseT, runResponseT)
import           Profile
import           System.Environment    (getArgs)
import           Trade

main :: IO ()
main = do
  (command:name:args) <- getArgs
  case command of
    "trade" -> do
      keys <- readProfile name
      forever $ printStatus $ trade keys args
      return ()
    "newProfile" -> createProfile name

printStatus :: ResponseT () -> IO ()
printStatus r = do
  status <- runResponseT r
  case status of
    Right _    -> putStrLn "Всё в порядке"
    Left error -> putStrLn $ "Ошибка: " ++ error
