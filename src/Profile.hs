module Profile
    ( createProfile
    , readProfile
    ) where

import           Crypto.Cipher.AES
import           Crypto.Cipher.Types
import           Crypto.Error (CryptoFailable (..))
import qualified Data.ByteString.Char8 as BS
import           Livecoin (Keys)
import qualified System.Console.Haskeline as HL

createProfile :: String -> IO ()
createProfile name = 
    ( encrypt
    <$>
    fmap generateKey getPassword
    <*>
    (pairKeys <$> getAPIKey <*> getSecretKey))
    >>=
    BS.writeFile (name ++ ext)

    where
        pairKeys :: String -> String -> Keys
        pairKeys api secret = (BS.pack api, BS.pack secret)

        encrypt :: Crypto -> Keys -> BS.ByteString
        encrypt key = ecbEncrypt key . fillBlock . BS.pack . show

        fillBlock :: BS.ByteString -> BS.ByteString
        fillBlock string =
            if ((BS.length string) `mod` 16) == 0
            then string
            else fillBlock $ string `BS.snoc` ' '

readProfile :: String -> IO Keys
readProfile name =
    decrypt
    <$>
    fmap generateKey getPassword
    <*>
    BS.readFile (name ++ ext)

    where
        decrypt :: Crypto -> BS.ByteString -> Keys
        decrypt key = read . BS.unpack . ecbDecrypt key

generateKey :: String -> Crypto
generateKey password =
    if (l == size)
    then cipher rawKey
    else if (l < size)
         then cipher $ rawKey `BS.append` (BS.replicate (size-l) ' ')
         else cipher $ BS.take size rawKey
    
    where
        rawKey = BS.pack password

        cipher :: BS.ByteString -> Crypto
        cipher string = case cipherInit string of
            CryptoPassed key -> key

        size :: Int
        size = takeSize $ cipherKeySize (undefined :: Crypto)

        takeSize :: KeySizeSpecifier -> Int
        takeSize (KeySizeFixed size) = size

        l = BS.length rawKey

type Crypto = AES256

ext :: String
ext = ".pr"

getPassword :: IO String
getPassword = getKey "Please enter the password: "

getAPIKey :: IO String
getAPIKey = getKey "Please enter the API key: "

getSecretKey :: IO String
getSecretKey = getKey "Please enter the secret key: "

getKey :: String -> IO String
getKey prompt = HL.runInputT HL.defaultSettings $ do
    key <- HL.getPassword Nothing prompt
    case key of
        Just string -> return string
