module Trade
  ( trade
  ) where

import           Control.Concurrent        (threadDelay)
import           Control.Monad.Trans.Class (lift)
import           Data.List                 (sortOn)
import           Data.Time.Clock.POSIX
import qualified Livecoin                  as LC

trade :: LC.Keys -> [String] -> LC.ResponseT ()
trade keys (pair:[]) = do
  clientOrders <-
    LC.clientOrders keys $
    (Just pair, Just LC.Open, Nothing, Nothing, Nothing, Nothing)
  lift delay
  sellBalance <- LC.balances keys $ Just $ fst $ splitPair pair
  buyBalance <- LC.balances keys $ Just $ snd $ splitPair pair
  book <- LC.orderBook (pair, Just True, Nothing)
  history <- LC.trades keys (Just pair, Just True, Nothing, Nothing)
  let sellOrders = filter isSellOrder $ LC.ordersData clientOrders
  let buyOrders = filter isBuyOrder $ LC.ordersData clientOrders
  let availableQuantity = LC.balanceValue $ availableBalance sellBalance
  let availableVolume = LC.balanceValue $ availableBalance buyBalance
  let fullQuantity = LC.balanceValue $ fullBalance sellBalance
  let fullVolume = LC.balanceValue $ fullBalance buyBalance
  let sellHistory = filter wasSell history
  let buyHistory = filter wasBuy history
  let filteredAsks = filterAsks fullQuantity buyHistory $ LC.asks book
  let filteredBids = filterBids fullVolume sellHistory $ LC.bids book
  (fand <$> (doSell $ sellAction sellOrders availableQuantity filteredAsks) <*>
   (doBuy $ buyAction buyOrders availableVolume filteredBids))
  where
    doSell :: Trade -> LC.ResponseT ()
    doSell Inaction = lift $ return ()
    doSell (Cancel orderID) = do
      LC.cancelLimit keys (pair, orderID)
      lift delay
    doSell (Trade price quantity) = do
      LC.sellLimit keys (pair, price, quantity)
      lift delay
    doBuy :: Trade -> LC.ResponseT ()
    doBuy Inaction = lift $ return ()
    doBuy (Cancel orderID) = do
      LC.cancelLimit keys (pair, orderID)
      lift delay
    doBuy (Trade price quantity) = do
      LC.buyLimit keys (pair, price, quantity)
      lift delay

data Trade
  = Inaction
  | Cancel Integer
  | Trade LC.F50 LC.F50

commission :: LC.F50
commission = 0.0018

filterAsks :: LC.F50 -> LC.History -> [LC.Order] -> [LC.Order]
filterAsks quantity history asks =
  filter (isOrderPriceGreater (goodSellPrice quantity history)) asks

filterBids :: LC.F50 -> LC.History -> [LC.Order] -> [LC.Order]
filterBids volume history bids =
  filter (isOrderPriceLesser (goodBuyPrice volume history)) bids

goodSellPrice :: LC.F50 -> LC.History -> LC.F50
goodSellPrice quantity history =
  (averageBuyPrice quantity history) / ((1 - commission) ^ 2)

goodBuyPrice :: LC.F50 -> LC.History -> LC.F50
goodBuyPrice volume history =
  (averageSellPrice volume history) * ((1 - commission) ^ 2)

isOrderPriceGreater :: LC.F50 -> LC.Order -> Bool
isOrderPriceGreater price order = (price == 0) || (LC.price order > price)

isOrderPriceLesser :: LC.F50 -> LC.Order -> Bool
isOrderPriceLesser price order = (price == 0) || (LC.price order < price)

averageSellPrice :: LC.F50 -> LC.History -> LC.F50
averageSellPrice _ [] = 0
averageSellPrice volume history =
  if volume == 0
    then 0
    else volume / ((sellQuantity volume history) * (1 - commission))
  where
    sellQuantity :: LC.F50 -> LC.History -> LC.F50
    sellQuantity v (h:hs) =
      if hv < v
        then hq + (sellQuantity (v - hv) hs)
        else v / hp
      where
        hq = LC.historyQuantity h
        hp = LC.historyPrice h
        hv = hq * hp

averageBuyPrice :: LC.F50 -> LC.History -> LC.F50
averageBuyPrice _ [] = 0
averageBuyPrice quantity history =
  if quantity == 0
    then 0
    else ((buyVolume quantity history) * (1 - commission)) / quantity
  where
    buyVolume :: LC.F50 -> LC.History -> LC.F50
    buyVolume q (h:hs) =
      if (hq < q)
        then hv + (buyVolume (q - hq) hs)
        else q * hp
      where
        hq = LC.historyQuantity h
        hp = LC.historyPrice h
        hv = hp * hq

splitPair :: String -> (String, String)
splitPair pair = (fst splittedPair, tail $ snd splittedPair)
  where
    splittedPair = break ((==) '/') pair

sellAction :: [LC.ClientOrder] -> LC.F50 -> [LC.Order] -> Trade
sellAction [] balance asks =
  if ((balance * bestPrice) <= 1.0001) -- temporary workaround
    then Inaction
    else Trade bestPrice balance
  where
    bestPrice = sellPrice balance asks
sellAction (order:[]) balance asks =
  if (((balance * bestPrice) >= 1.0001) || (not $ isBestSellPrice order asks)) -- temporary workaround
    then Cancel $ LC.clientOrderID order
    else Inaction
  where
    bestPrice = sellPrice balance asks

buyAction :: [LC.ClientOrder] -> LC.F50 -> [LC.Order] -> Trade
buyAction [] balance bids =
  if (balance < 1.0001) -- temporary workaround
    then Inaction
    else Trade bestPrice (roundTo8 quantity)
  where
    bestPrice = buyPrice balance bids
    quantity = (balance / bestPrice) * (1 - commission)
    roundTo8 :: LC.F50 -> LC.F50
    roundTo8 n = (fromIntegral $ floor $ n * 10e7) / 10e7
buyAction (order:[]) balance bids =
  if ((balance >= 1.0001) || (not $ isBestBuyPrice order bids)) -- temporary workaround
    then Cancel $ LC.clientOrderID order
    else Inaction

sellPrice :: LC.F50 -> [LC.Order] -> LC.F50
sellPrice _ [] = 1000000
sellPrice q (ask:asks) =
  if (LC.quantity ask) >= q
    then (LC.price ask) - 0.00000001
    else sellPrice q asks

buyPrice :: LC.F50 -> [LC.Order] -> LC.F50
buyPrice _ [] = 0.00000001
buyPrice v (bid:bids) =
  if (volume bid) >= v
    then (LC.price bid) + 0.00000001
    else buyPrice v bids
  where
    volume :: LC.Order -> LC.F50
    volume bid = (LC.price bid) * (LC.quantity bid)

isBestSellPrice :: LC.ClientOrder -> [LC.Order] -> Bool
isBestSellPrice order asks =
  price == sellPrice quantity (filter (not . isThatPrice price) asks)
  where
    price = LC.clientOrderPrice order
    quantity = LC.clientOrderQuantity order

isBestBuyPrice :: LC.ClientOrder -> [LC.Order] -> Bool
isBestBuyPrice order bids =
  price == buyPrice (volume order) (filter (not . isThatPrice price) bids)
  where
    price = LC.clientOrderPrice order
    quantity = LC.clientOrderQuantity order
    volume :: LC.ClientOrder -> LC.F50
    volume order = price * quantity

isSellOrder :: LC.ClientOrder -> Bool
isSellOrder order =
  case (LC.clientOrderType order) of
    "LIMIT_SELL" -> True
    "LIMIT_BUY"  -> False

isBuyOrder :: LC.ClientOrder -> Bool
isBuyOrder = not . isSellOrder

wasSell :: LC.HistoryOrder -> Bool
wasSell order =
  case (LC.historyType order) of
    "sell" -> True
    "buy"  -> False

wasBuy :: LC.HistoryOrder -> Bool
wasBuy = not . wasSell

isThatPrice :: LC.F50 -> LC.Order -> Bool
isThatPrice price order = price == (LC.price order)

availableBalance :: LC.Balances -> LC.Balance
availableBalance (b:bs) =
  case (LC.balanceType b) of
    LC.Available -> b
    otherwise    -> availableBalance bs

fullBalance :: LC.Balances -> LC.Balance
fullBalance (b:bs) =
  case (LC.balanceType b) of
    LC.Total  -> b
    otherwise -> fullBalance bs

checkStatus :: LC.OrderData -> ()
checkStatus _ = ()

fand :: () -> () -> ()
fand _ _ = ()

delay :: IO ()
delay = threadDelay 2000000
